load('overhead_01')
oh01 = overhead;
load('overhead_5')
oh5 = overhead;

%% Overheads of the service BC for lambda = {0.1, 5}
figure
subplot(1,3,1)
bar([oh01(1,:)'./300e6 oh5(1,:)'./300e6], 'stacked');
axis([0 11 0 2.4])
set(gca,'FontSize',18,'FontName','Times')
title('\lambda = 1')
grid on
grid minor
xlabel('Block size (# trans.)')
ylabel('Overhead (bps)')
legend({'T_w = 0.1s', 'T_w = 5s'})
subplot(1,3,2)
bar([oh01(2,:)'./300e6 oh5(2,:)'./300e6], 'stacked');
axis([0 11 0 2.4])
set(gca,'FontSize',18,'FontName','Times')
title('\lambda = 5')
grid on
grid minor
xlabel('Block size (# trans.)')
ylabel('Overhead (bps)')
legend({'T_w = 0.1s', 'T_w = 5s'})
subplot(1,3,3)
bar([oh01(3,:)'./300e6 oh5(3,:)'./300e6], 'stacked');
axis([0 11 0 2.4])
set(gca,'FontSize',18,'FontName','Times')
title('\lambda = 10')
grid on
grid minor
xlabel('Block size (# trans.)')
ylabel('Overhead (bps)')
legend({'T_w = 0.1s', 'T_w = 5s'})