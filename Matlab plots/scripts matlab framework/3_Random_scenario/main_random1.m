%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
clear
close all
clc

% Load constants
constants
conf_environment_random1

num_deployments = 10;

ops = [2 5 10 15];
%%
for nOperators = ops
    ownedApsRatio = zeros(1, nOperators);
for k = 1 : num_deployments
    disp(['Deployment ' num2str(k) '/' num2str(num_deployments)])
    % Ratio of APs owned by each operator
    for o = 1 : nOperators 
        if o < nOperators
            ownedApsRatio(o) = 1/nOperators;
        else
           ownedApsRatio(o) = 1-sum(ownedApsRatio(1:end-1)); 
        end
    end
%     ownedApsRatio(1) = 1;
%     ownedApsRatio(2) = 0;
    
%     if nOperators == 2
%     ownedApsRatio(1) = rand(1); 
%     ownedApsRatio(2) = 1 - ownedApsRatio(1);
%     elseif nOperators == 3
%         ownedApsRatio(1) = rand(1); 
%         ownedApsRatio(2) = (1 - ownedApsRatio(1))*rand(1);
%         ownedApsRatio(3) = 1 - ownedApsRatio(1) - ownedApsRatio(2);
%     elseif nOperators == 4
%         ownedApsRatio(1) = rand(1);
%         ownedApsRatio(2) = (1 - ownedApsRatio(1))*rand(1);
%         ownedApsRatio(3) = (1 - ownedApsRatio(1) - ownedApsRatio(2))*rand(1);
%         ownedApsRatio(4) = 1 - ownedApsRatio(1) - ownedApsRatio(2) - ownedApsRatio(3);
%     elseif nOperators == 5
%         ownedApsRatio(1) = rand(1);
%         ownedApsRatio(2) = (1 - ownedApsRatio(1))*rand(1);
%         ownedApsRatio(3) = (1 - ownedApsRatio(1) - ownedApsRatio(2))*rand(1);
%         ownedApsRatio(4) = (1 - ownedApsRatio(1) - ownedApsRatio(2) - ownedApsRatio(3))*rand(1);
%         ownedApsRatio(5) = 1 - ownedApsRatio(1) - ownedApsRatio(2) - ownedApsRatio(3) - ownedApsRatio(4);
%     end
    
    % Generate the deployment
    deployment = GenerateDeployment(nStas);
    % Create MNOs (assign resources to BSs) and users
    [deployment, operators] = CreateOperators(deployment, nOperators, ownedApsRatio);
    users = GenerateUsers(deployment);
    % Determine miners and computational power
    [deployment, miners] = InitializeMiners(deployment);   
    %if PLOTS_ENABLED, DrawDeployment(deployment); end
    if LOGS_ENABLED, PrintSimulationDetails(deployment, operators, users); end
    % Iterate for each value of user activity (number of requests per second)
    for a = 1 : length(service_auction_modes)
        %disp(['Service auction type = ' num2str(service_auction_modes(a))])
        for aa = 1 : length(spectrum_auction_modes)
            disp([' . Spectrum auction type = ' num2str(spectrum_auction_modes(aa))])
            for l = 1 : length(lambda)
                % Iterate for each value of user activity (number of requests per second)
                disp(['   + Users arrivals (lambda) = ' num2str(lambda(l))])
                % Iterate for each block timeout
                for t = 1 : length(block_timeout)
                    %disp(['     * Block timeout = ' num2str(block_timeout(t))])
                    % Iterate for each block size
                    for s = 1 : length(block_size)    
                        MAX_BLOCK_SIZE = TRANSACTION_LENGTH*block_size(s);       % Maximum block size in bits                
                        % RUN THE SIMULATION
                        RunSimulation(deployment, operators, users, miners, service_auction_modes(a), ...
                            spectrum_auction_modes(aa), lambda(l), block_timeout(t), TRANSACTION_LENGTH*block_size(s), k);                      
                    end % end "for" block size values
                end % end "for" timer values      
            end % end "for" lambda values  
        end % end "for" spectrum auction modes    
    end % end "for" service auction modes  
end
end

%% Plot results
%if PLOTS_ENABLED
%    PlotResults(service_auction_modes, spectrum_auction_modes, lambda, block_timeout, TRANSACTION_LENGTH.*block_size);
%end